#include <stdlib.h>
#include <string.h>

extern "C"
{
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

#pragma comment(lib, "Lua5.2.lib")

int main()
{
	char buf[256];
	int error;
	lua_State *l = luaL_newstate();
	luaL_openlibs(l);
	while(fgets(buf, sizeof(buf), stdin) != NULL)
	{
		error = luaL_loadbuffer(l, buf, strlen(buf), "line") || lua_pcall(l, 0, 0, 0);
		if(error)
		{
			fprintf(stderr, "%s", lua_tostring(l, -1));
			lua_pop(l, 1);
		}
	}
	lua_close(l);
	return 0;
}
