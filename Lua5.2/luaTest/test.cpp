#include <iostream>
#include "../Lua5.2/src/lua.hpp"

using namespace  std;

#pragma comment(lib, "Lua5.2.lib")

int c_test(lua_State *l)
{
	printf("c_test1:%s\n",lua_tostring(l, 1));
	printf("c_test2:%d\n",lua_tointeger(l, 2));
	lua_pushnumber(l, 10000);
	return 1;
}

int main()
{
	lua_State *l = luaL_newstate();
	luaL_openlibs(l);
	//lua_register(l, "c_test", c_test);
	lua_pushcfunction(l, c_test);
	lua_setglobal(l,"c_test");
	luaL_dofile(l, "test.lua");

	lua_getglobal(l,"ts");
	lua_pushstring(l, "hello");
	lua_pushnumber(l, 1234);
	lua_call(l, 2, 2);

	printf("result1:%s\n", lua_tostring(l, -2));
	printf("result2:%s\n", lua_tostring(l, -1));

	lua_close(l);
	system("pause");
	return 0;
}