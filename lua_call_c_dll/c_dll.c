#include <stdio.h>
#include <string.h>

#include <lua.h>
#include <lualib.h>
#include <luaconf.h>
#include <lauxlib.h>

int add(lua_State *L)
{
	double op1 = luaL_checknumber(L, 1);
	double op2 = luaL_checknumber(L, 2);
	lua_pushnumber(L, op1+op2);
	return 1;
}

int sub(lua_State *L)
{
	double op1 = luaL_checknumber(L, 1);
	double op2 = luaL_checknumber(L, 2);
	lua_pushnumber(L, op1-op2);
	return 1;
}

__declspec(dllexport)
int luaopen_mytestlib(lua_State *L)
{
	static luaL_Reg mylibs[] = { 
		{"add", add},
		{"sub", sub},
		{NULL, NULL} 
	};

	luaL_newlib(L,mylibs);
	return 1;
}

